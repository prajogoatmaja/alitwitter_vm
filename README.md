# Ali Twitter

Twitter-like web app which can:

1. Create a tweet
2. Delete a tweet
3. List tweets

## Environment Setup
Please refer to the respective official documentation for installation guide.

- [Virtual Box](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/intro/getting-started/install.html)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## How to Run
1. Run this command to create the Virtual Machines
   ```bash
    vagrant up
   ```
2. Provision the installation and setup of the VMs using `ansible-playbook`
   ```bash
   ansible-playbook -i=ansible/inventory/hosts.yml ansible/install_ruby.yml

   ansible-playbook -i=ansible/inventory/hosts.yml ansible/setup_db.yml

   ansible-playbook -i=ansible/inventory/hosts.yml ansible/setup_lb.yml      
   ```

3. Install and setup your `gitlab-runner` inside the `runner VM` by running this code in the terminal.
    ```
    vagrant ssh runner
    ```
    Please follow gitlab's [official documentation](https://docs.gitlab.com/runner/install/linux-repository.html) regarding the installation and the setup.

## Open the Application
You can open the application by accessing Load Balancer's IP.
- Access `192.168.100.201/tweets` in your browser to see the Alitwitter Application.
- Access `192.168.100.201:1936` in your browser to see HAProxy Statistic Report on ALi Twitter Application
