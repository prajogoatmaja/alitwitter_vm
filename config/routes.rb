Rails.application.routes.draw do
  resources :tweets, only: %i[index new create destroy]
end
