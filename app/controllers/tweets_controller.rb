class TweetsController < ApplicationController
  def index
    @tweets = Tweet.order(created_at: :desc).all
  end

  def new
    @tweet = Tweet.new
  end

  def create
    @tweet = Tweet.new(tweet_params)
    if @tweet.save
      redirect_to tweets_url, notice: 'Tweet was successfully created!'
    else
      render 'new'
    end
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy
    redirect_to tweets_url, notice: 'Tweet was successfully deleted!'
  end

  private
  def tweet_params
    params.require(:tweet).permit(:message)
  end
end
